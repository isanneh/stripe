# Stripe Tutorial by Isatou Sanneh

import stripe
from flask import Flask
from flask import render_template
from flask import request
app = Flask(__name__)

# Note: price is in cents, not dollars
tutorials = [
    {
        "author": "Arun Ramsey",
        "title": "Alexa API",
        "price": 500,
        "image": "static/alexa.png",
        "presentation": "https://goo.gl/BC1F87",
        "tutorial": "https://github.com/arunras/alexa-rainbow",
    },
    {
        "author": "Avinash Bengeri",
        "title": "BotPress",
        "price": 500,
        "image": "static/botpress.png",
        "presentation": "https://drive.google.com/open?id=0B45WrEizfvkEZzRJVVhSRzZTLVk",
        "tutorial": "https://drive.google.com/open?id=0B45WrEizfvkEQTFmXzFHLWdHTWM",

    },
    {
        "author": "Anamika Singh",
        "title": "Sails.js",
        "price": 500,
        "image": "static/sailsjs.png",
        "presentation": "https://drive.google.com/drive/folders/0B2_DNlJjpmUSZmNkM180ZHQ3ejQ",
        "tutorial": "https://github.com/asingh015/SailsDemo",
    },
    {
        "author": "Deekshith Polusani",
        "title": "TypeScript",
        "price": 500,
        "image": "static/typescript.png",
        "presentation": "https://drive.google.com/open?id=0B6xfBmpX_pDJYkE2VjBrbzVvbk0",
        "tutorial": "https://github.com/deekshithraop/my-app",
    },
    {
        "author": "Sangeeta Ashrit",
        "title": "GraghQL",
        "price": 500,
        "image": "static/graphql.png",
        "presentation": "https://goo.gl/VZR6jU",
        "tutorial": "https://github.com/sangeeta2288/GitHub-GraphQL-Server",
    },
    {
        "author": "Amandeep Singh",
        "title": "React Native",
        "price": 500,
        "image": "static/react.png",
        "presentation": "http://bit.ly/presentation-files-keynote",
        "tutorial": "https://github.com/amandeeepsingh/TipCalculator-ReactNative",
    },
    {
        "author": "Fahad Uddin",
        "title": "Phaser.js",
        "price": 500,
        "image": "static/phaser.png",
        "presentation": "https://github.com/fahadun/phaser_redo/blob/master/README.md",
        "tutorial": "https://github.com/fahadun/phaser_redo",
    },
    {
        "author": "Vladimir Toussaint",
        "title": "Ionic Framework",
        "price": 500,
        "image": "static/ionic.png",
        "presentation": "https://docs.google.com/presentation/d/16jhH4dzeiX6EvIFA4qX-4M8g4TdUjKJLT5qNZepIf-g/edit#slide=id.p",
        "tutorial": "https://github.com/vtoussa000/IonicFrameworkv2.git",
    },
    {
        "author": "Luis Villamarin",
        "title": "Vue.js",
        "price": 500,
        "image": "static/vuejs.png",
        "presentation": "https://docs.google.com/presentation/d/1_0Iflj_vLaBsFcPc1YliNDaBtHSQ9b1D7qJnHn_toEk/edit?usp=sharing",
        "tutorial": "https://github.com/lv10/mav",
    },
    {
        "author": "Isatou Sanneh",
        "title": "TensorFlow",
        "price": 500,
        "image": "static/tensorflow.png",
        "presentation": "https://docs.google.com/presentation/d/13m4S6c2VID_j1q3dr-c_CRCS86VL_tCYzJ-y9Nd-pPc/edit?usp=sharing",
        "tutorial": "https://bitbucket.org/isanneh/fooddeliveryestimates",
    },
    {
        "author": "Bharat Rosanlall",
        "title": "D3",
        "price": 500,
        "image": "static/d3.png",
        "presentation": "https://bitbucket.org/brosanlall93/batteryvisualization/src/c184b52e717a6eb2d432dee41ba72a5f7ae3a588/presentation/D3%20SVG%20Presentation.pdf?at=master&fileviewer=file-view-default",
        "tutorial": "https://brosanlall93@bitbucket.org/brosanlall93/batteryvisualization.git",
    }

]


@app.route('/', methods=['POST', 'GET'])
def payment():
    paid = False
    paid_tutorial = []
    error = False
    error_message = ""
    
    if request.method == 'POST':
        # Get the payment token submitted by the form:
        token = request.form['stripeToken'] # Using Flask
        
        # token
        token = request.form['stripeToken']
        
        # title
        title = request.form['title']
        
        # email
        email = request.form['stripeEmail']
        
        price = request.form['amount']
        
        # API key
        stripe.api_key = "sk_test_gcfLCrE8rPPntw9WIbX0VFjE"
    
        try:
        
            # Use Stripe's library to make requests...
            # charge user
            charge = stripe.Charge.create(
                amount=price,
                currency="usd",
                source= token,
                description="Charged %s for %s" %(email, title)
            )
            
        
            if charge.paid:
                
                # if no error, that means payment was successful
                paid = True
            
                # get tutorial
                for tutorial in tutorials:
                    if tutorial['title'] == title:
                        paid_tutorial = tutorial
            else:
                error = True
                error_message = "Sorry, an error occurred. Your payment could not be processed."
                
                return render_template(
                    'index.html',
                    tutorials=tutorials,
                    paid=paid,
                    paid_tutorial=paid_tutorial,
                    error = error,
                    error_message = error_message
                )

        except stripe.error.CardError as e:
            # Since it's a decline, stripe.error.CardError will be caught
            error = True
            error_message = err['message']
        except stripe.error.StripeError as e:
            # Display a very generic error to the user
            error = True
            error_message = "Sorry, an error occurred. Your payment could not be processed."
        except Exception as e:
            # Something else happened, completely unrelated to Stripe
            print "error", e
            error = True
            error_message = "Sorry, an error occurred. Your payment could not be processed."


    return render_template(
               'index.html',
                tutorials=tutorials,
                paid=paid,
                paid_tutorial=paid_tutorial,
                error = error,
                error_message = error_message
            )

if __name__ == "__main__":
    app.run()






