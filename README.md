# Stripe Tutorial by Isatou Sanneh

In this application, we use stripe to sell content on different Web Technologies.  These materials were taken from the Advanced Internet Programming Course taught by Professor Michael Grossberg at the City College of New York.

You need to install the following for this tutorial:

* The latest version of [Python](https://www.python.org/)
*  [Stripe](https://stripe.com)

        pip install --upgrade stripe

* [Flask](http://flask.pocoo.org/)

        pip install flask

----
## Generate Stripe API keys
You can generate an API key for Stripe [here](https://dashboard.stripe.com/account/apikeys).
Stripe automatically creates a test API key for you once your create an account. Use the test  API for testing purposes.

----
## Authentication

    import stripe
    stripe.api_key = "<INSERT-API-KEY>"

Replace <INSERT-API-KEY> with your actual API key.

----
## Payment Form

>Checkout

>The easiest way to integrate Stripe is via Checkout, an embedded tool that takes care of building an HTML form, validating user input, and securing your customers' card data.

You can read more about Checkout [here](https://stripe.com/docs/checkout/tutorial)


Use stripe.js instead if you want complete control over how you want your payment form to look like.

>Stripe.js is used to securely send sensitive information from the customer’s browser to Stripe.

You can read more about Stripe.js [here](https://stripe.com/docs/stripe.js)

We use Stripe's Checkout for our payment form

      <div class="row">
        {% for tutorial in tutorials %}
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <img src="{{ tutorial.image }}" class="image">
              <div class="caption">
                <p>Author: {{ tutorial.author }}</p>
                <p>Price: ${{ tutorial.price/100 }}</p>
                {% if tutorial != paid_tutorial %}
                  <form action="/" method="POST" align="center">
                    <input id="title" name="title" type="hidden" value="{{ tutorial.title }}">
                    <input id="amount" name="amount" type="hidden" value="{{ tutorial.price }}">
                    <script
                      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                      data-key="pk_test_02sYlq20tWN23NxOd1Pz3GIJ"
                      data-amount="500"
                      data-name="Advanced Internet Programming Collection"
                      data-description="{{ tutorial.title }}"
                      data-image="{{ tutorial.image }}"
                      data-label="Buy {{ tutorial.title }}"
                      data-locale="auto">
                    </script>
                  </form>
                {% endif %}
              </div>

We use the POST method for our form.  The form is handled in the base url, "/" by the payment view in our flask app.

     <form action="/" method="POST" align="center">

----
## Form Post Data
We get the data from our payment form here.

    if request.method == 'POST':
        # Get the payment token submitted by the form:
        token = request.form['stripeToken'] # Using Flask
        
        # token
        token = request.form['stripeToken']
        
        # title
        title = request.form['title']
        
        # email
        email = request.form['stripeEmail']
        
        price = request.form['amount']

----
## Charge User

We charge the user here using data from the payment form:

            # Use Stripe's library to make requests...
            # charge user
            charge = stripe.Charge.create(
                amount=price,
                currency="usd",
                source= token,
                description="Charged %s for %s" %(email, title)
            )

Note: The value you assign to assign should be in cents, instead of dollars. E.g. amount = 500 means 500 cents, which is 5 dollars.

You can read more about the Charges API [here](https://stripe.com/docs/api#charges)

----
## Error Checking
You need to check for errors and handle any exceptions that may occur when you use the charge API. If there's no error, that probably means the charge was successful.  You can also confirm the charge the charge was successful by checking if charge.paid is True.

We handle our errors here:

        except stripe.error.CardError as e:
            # Since it's a decline, stripe.error.CardError will be caught
            error = True
            error_message = err['message']
        except stripe.error.StripeError as e:
            # Display a very generic error to the user
            error = True
            error_message = "Sorry, an error occurred. Your payment could not be processed."
        except Exception as e:
            # Something else happened, completely unrelated to Stripe
            error = True
            error_message = "Sorry, an error occurred. Your payment could not be processed."

----
## Stripe no longer supports API requests made with TLS 1.0. 
If you get the following error:
>Stripe no longer supports API requests made with TLS 1.0. Please initiate HTTPS connections with TLS 1.2 or later. You can learn more about this at https://stripe.com/blog/upgrading-tls.

Make sure you install the latest version of python that supports TLS1 1.2 or later.

You can read more about it [https://stripe.com/blog/upgrading-tls](https://stripe.com/blog/upgrading-tls) and [https://support.stripe.com/questions/how-do-i-upgrade-my-stripe-integration-from-tls-1-0-to-tls-1-2#python](https://support.stripe.com/questions/how-do-i-upgrade-my-stripe-integration-from-tls-1-0-to-tls-1-2#python)

----
## Testing
Stripe provides several testing cards that you can use to test your application while it is in test mode.

You can use this card to test our application:

Card number: 4242 4242 4242 4242
	
Card Type: Visa

You use any 3-digit number for the CVC.

You can use any date in the future as the expiration date.

You can read more about testing [here](https://stripe.com/docs/testing)